const formGetBan = document.querySelector("form.get--ban");
const buttonGetBan = document.querySelector("button.get--ban");
const closeButton = document.querySelector(".close");
const infoGetBan = document.querySelector(".information--getban");
const cancelSwap = document.querySelector(".cancel--ban");
const inputGetBan = document.querySelector(".get--ban-address");

formGetBan.addEventListener("submit", (e) => {
  e.preventDefault();
  if (inputGetBan.value.length > 0) {
    infoGetBan.innerHTML = `<p class="break-words">Please send min. 
    <b>5000BAN</b></p>
    <p class="break-all mt-2"><strong>from:</strong> ${inputGetBan.value}</p>
    <p class="mt-2 break-all"><strong>to:</strong>
    ban_1wban3quebxo6q6wy796te3bu31hbcis8fq76hz8oqc96p1xr4pbxrkxyie6</p>`;

    infoGetBan.classList.remove("hidden");
    infoGetBan.classList.add("flex");
    cancelSwap.classList.remove("hidden");
    inputGetBan.classList.add("hidden");
    buttonGetBan.classList.add("hidden");
    buttonGetBan.classList.add("hidden");
    cancelSwap.addEventListener("click", (e) => {
      infoGetBan.classList.add("hidden");
      cancelSwap.classList.add("hidden");
      inputGetBan.classList.remove("hidden");
      buttonGetBan.classList.remove("hidden");
      buttonGetBan.classList.remove("hidden");
    });
  }
});
