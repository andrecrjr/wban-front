module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === "production" ? true : false,
    content: ["./src/**/*.html"],
  },
  theme: {
    extend: {
      width: {
        w80: "80%",
      },
      fontFamily: {
        "now-alt": "NowAlt",
      },
      boxShadow: {
        "shadow-swap": "0px 2px 15px #00000026;",
        "shadow-button": "0px 3px 6px #00000029;",
      },
      backgroundColor: {
        "banano-green": "#4CBF4B",
        "banano-yellow": "#FBDD11",
        "banano-green-2": "#1FE080",
      },
      height: {
        27: "7rem",
      },
      fontSize: {
        "3xs": ".6rem",
        "2xs": "14px",
      },
      minHeight: {
        8: "8rem",
      },
    },
  },
  variants: {},
  plugins: [],
};
