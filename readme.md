# Run the project

- yarn install or npm install

- npm run build:dev - watching dev webpack
- npm run build:prd - generate build prd in dist folder

---

# Add a new script

- Just put your js inside src folder, and put him in the webpack's entry
- Your dist build will need to have inside each script, so put them in the index.html to work finely
- Tailwind in dev environment is slower because of a lot of style that came with the library, but helps you with working for front-end, though, if you just work with scripts, you can disable by default in the "purge" part of the object in the tailwind.config.js.
